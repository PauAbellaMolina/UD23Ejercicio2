package es.http.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud23Ejercicio2Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud23Ejercicio2Application.class, args);
	}

}
